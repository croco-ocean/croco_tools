# Changelog

Release changelog are available here : https://gitlab.inria.fr/croco-ocean/croco_tools/-/releases

## [x.x.x] - xxxx-xx-xx

### Added

## Fixed

- Issue 50 : fix in mexcdf functionality for matlab 20xx versions (in particular matlab 2024)
- Issue 49, 43 : add GSHHS coastline dataset in m_map private directory
- Issue 44 : fix problem in editmask with longitude in 0-360° convention
- Issue 34 : fix redef and endef issue in Nesting tools + some cleaning in nestgui

## Changed

## Removed

### Deprecated

### Other

